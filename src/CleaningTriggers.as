package  
{
	/**
	 * ...
	 * @author Pimgd
	 */
	public class CleaningTriggers extends CustomTriggerHolder
	{
		public function CleaningTriggers(G:Object, M:Main) 
		{
			super(G, M);
		}
		
		public function registerTriggers(t:TriggerManager):void {
			t.registerTrigger("CLEAN_CUM", 0, new FunctionObject(cleanCum, this, []));
			t.registerTrigger("CLEAN_MASCARA", 0, new FunctionObject(cleanMascara, this, []));
			t.registerTrigger("CLEAN_SPIT", 0, new FunctionObject(cleanSpit, this, []));
			t.registerTrigger("CLEAN_LIPSTICK", 0, new FunctionObject(cleanLipstick, this, []));
			t.registerTrigger("CLEAN_ALL", 0, new FunctionObject(cleanAll, this, []));
		}
		
		public function cleanCum(...args):void {
			g.strandControl.clearCumStrands();
		}
		public function cleanSpit(...args):void {
			g.strandControl.clearSpitStrands();
		}
		public function cleanLipstick(...args):void {
			g.him.clearLipstick();
		}
		public function cleanMascara(...args):void {
			g.her.clearMascara();
		}
		public function cleanAll(...args):void {
			cleanCum();
			cleanSpit();
			cleanLipstick();
			cleanMascara();
		}
		
	}

}
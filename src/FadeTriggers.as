package  
{
	/**
	 * ...
	 * @author Pimgd
	 */
	public class FadeTriggers extends CustomTriggerHolder
	{
		private var fader:Fader;
		private var functionObject:FunctionObject;
		public function FadeTriggers(G:Object, M:Main) 
		{
			super(G, M);
		}
		
		public function registerTriggers(t:TriggerManager):void {
			t.registerTrigger("INSTANT_FLASH", 1, new FunctionObject(instantFlashScreen, this, []));
			t.registerTrigger("INSTANT_FLASH", 2, new FunctionObject(instantFlashScreenWithDuration, this, []));
			t.registerTrigger("FLASH", 1, new FunctionObject(flashScreen, this, []));
			t.registerTrigger("FLASH", 2, new FunctionObject(flashScreenWithDuration, this, []));
			t.registerTrigger("END_FLASH", 0, new FunctionObject(endFlash, this, []));
			t.registerTrigger("INSTANT_END_FLASH", 0, new FunctionObject(instantEndFlash, this, []));
			t.registerTrigger("FLASH_CHANGE_COLOR", 1, new FunctionObject(changeFlashColor, this, []));
			t.registerTrigger("FADE_BACKGROUND", 1, new FunctionObject(flashScreenBackground, this, []));
			//[FLASH_<hex>], [FLASH_<hex>_<timeInMS>], [END_FLASH], [INSTANT_END_FLASH]
			//[FLASH_CHANGE_COLOR_<hex>]
			//addChildAt(object, getChildIndex(objectForeground))
		}
		
		public function flashScreen(hex:String):void {
			flashScreenWithDuration(hex, -1);
		}
		
		public function flashScreenBackground(hex:String):void {
			flashScreenWithDuration(hex, -1);
			var f:FunctionObject = m.getTriggerManager().getFunctionObjectForTrigger("CHANGE_BACKGROUND", 0);
			fader.setTriggerObject(new FunctionObject(f.call, f, []));
		}
		
		public function instantFlashScreen(hex:String):void {
			instantFlashScreenWithDuration(hex, -1);
		}
		public function flashScreenWithDuration(...args):void {
			//m.displayMessageGreen("flashScreenWithDuration " + Main.arrayToString(args));
			if (args[0] is Array) {
				flashScreenWithDuration(args[0][0], args[0][1]);
			} else {
				flashScreenWithDurationInternal(args[0], args[1]);
			}
		}
		public function instantFlashScreenWithDuration(...args):void {
			if (args[0] is Array) {
				instantFlashScreenWithDuration(args[0][0], args[0][1]);
			} else {
				flashScreenWithDurationInternal(args[0], args[1]);
				fader.alpha = 1;
			}
			
		}
		public function flashScreenWithDurationInternal(hex:String, duration:int):void {
			attachAndDefineFaderIfNotExists();
			fader.setFadeDelay(duration);
			fader.fade(hex);
		}
		
		public function changeFlashColor(hex:String):void {
			attachAndDefineFaderIfNotExists();
			fader.setHexColor(hex);
		}
		
		public function endFlash(...args):void {
			attachAndDefineFaderIfNotExists();
			fader.endFade();
		}
		public function instantEndFlash(...args):void {
			attachAndDefineFaderIfNotExists();
			fader.instantEndFade();
		}
		
		private function attachAndDefineFaderIfNotExists():void {
			if (fader == null) {
				fader = new Fader();
				g.container.addChildAt(fader, g.container.getChildIndex(g.dialogueControl));
			}
		}
		
	}

}
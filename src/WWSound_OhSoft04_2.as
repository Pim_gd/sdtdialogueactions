package {
	
	import flash.media.Sound;
	
	[Embed(source="../audio/WWSounds_OhSoft04_2.swf",symbol="WWSound_OhSoft04_2")]
	
	public dynamic class WWSound_OhSoft04_2 extends Sound {
		
		public function WWSound_OhSoft04_2() {
			super();
		}
	}

}

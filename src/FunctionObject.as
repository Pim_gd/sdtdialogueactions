﻿package {
	/**
	 * ...
	 * @author Pimgd
	 */
	
	public class FunctionObject 
	{
		private var f:Function;
		private var o:Object;
		private var args:Array;
		public function FunctionObject(func:Function, obj:Object, fArgs:Array) {
			f = func;
			o = obj;
			args = fArgs;
			if (args == null) {
				args = new Array();
			}
		}
		public function call(...rest):* {
			var a:Array = args.concat();
			for each(var x in rest) {
				a.push(x);
			}
			var obj:Object = f.apply(o, a);
			if (obj != null) {
				return obj;
			}
		}
		public function apply(target:Object, ...rest):* {
			return f.apply(target, rest);
		}
		public function setArgs(nArgs:Array):void {
			args = nArgs.concat();
		}
		public function getArgs():Array {
			return args;
		}
		public function getFunction():Function {
			return f;
		}
	}
}
package  
{
	import flash.events.Event;
	/**
	 * ...
	 * @author Pimgd
	 */
	public class HimTriggers extends CustomTriggerHolder
	{
		public static const ORGASM_BLOCK_VARIABLE_NAME:String = "da.blockingOrgasm";
		private var blockingOrgasm:Boolean = false;
		public function HimTriggers(G:Object, M:Main) 
		{
			super(G, M);
		}
		
		public function registerTriggers(t:TriggerManager):void {
			t.registerTrigger("HIDE_HIM", 0, new FunctionObject(hideHim, this, []));
			t.registerTrigger("SHOW_HIM", 0, new FunctionObject(showHim, this, []));
			t.registerTrigger("HIDE_HIS_ARM", 0, new FunctionObject(hideHisArm, this, []));
			t.registerTrigger("SHOW_HIS_ARM", 0, new FunctionObject(showHisArm, this, []));
			t.registerTrigger("CUM", 0, new FunctionObject(cum, this, []));
			
			t.registerTrigger("CUM_BLOCK_ON", 0, new FunctionObject(enableCumBlock, this, []));
			t.registerTrigger("CUM_BLOCK_OFF", 0, new FunctionObject(disableCumBlock, this, []));
			
		}
		
		public function registerVariables(v:VariableManager):void {
			v.registerVariableRead(ORGASM_BLOCK_VARIABLE_NAME, new FunctionObject(isBlockingOrgasmDialogue, this, []));
			v.registerVariableWrite(ORGASM_BLOCK_VARIABLE_NAME, new FunctionObject(setBlockingOrgasm, this, []));
			v.registerVariableRead("da.him.ejaculating", new FunctionObject(isEjaculating, this, []));
			v.registerVariableRead("da.him.body.penis.minSize", new FunctionObject(getHimPenisMinSize, this, []));
			v.registerVariableRead("da.him.body.penis.maxSize", new FunctionObject(getHimPenisMaxSize, this, []));
			
			v.registerVariableGetterSetter("da.him.body.skin", new FunctionObject(getHimSkinType, this, []), new FunctionObject(setHimSkinType, this, []));
			v.registerVariableGetterSetter("da.him.body.gender", new FunctionObject(getHimGender, this, []), new FunctionObject(setHimGender, this, []));
			v.registerVariableGetterSetter("da.him.body.breasts", new FunctionObject(getHimBreastSize, this, []), new FunctionObject(setHimBreastSize, this, []));
			v.registerVariableGetterSetter("da.him.body.penis", new FunctionObject(getHimPenisType, this, []), new FunctionObject(setHimPenisType, this, []));
			v.registerVariableGetterSetter("da.him.body.penis.length", new FunctionObject(getHimPenisLength, this, []), new FunctionObject(setHimPenisLength, this, []));
			v.registerVariableGetterSetter("da.him.body.penis.width", new FunctionObject(getHimPenisWidth, this, []), new FunctionObject(setHimPenisWidth, this, []));
			v.registerVariableGetterSetter("da.him.body.balls", new FunctionObject(getHimBallsType, this, []), new FunctionObject(setHimBallsType, this, []));
			v.registerVariableGetterSetter("da.him.body.balls.size", new FunctionObject(getHimBallsize, this, []), new FunctionObject(setHimBallsize, this, []));
		}
		
		public function getHimPenisType(...args):String {
			return g.him.penisNameList[g.him.currentPenisID];
		}
		
		public function setHimPenisType(value:*):void {
			var penisIndex:int = g.him.penisNameList.indexOf(value);
			if (penisIndex == -1) {
				m.displayMessageRed("da.him.body.penis attempted set to " + value + ", unable to find penistype");
				return;
			}
			g.him.penisControl.select(penisIndex);
		}
		
		public function getHimPenisMinSize(...args):Number {
			return g.him.MIN_PENIS_SIZE;
		}
		
		public function getHimPenisMaxSize(...args):Number {
			return g.him.MAX_PENIS_SIZE;
		}
		
		public function getHimPenisWidth(...args):Number {
			return g.him.penis.scaleY;
		}
		
		public function setHimPenisWidth(value:*):void {
			var newVal:Number = calculateNewValue(new FunctionObject(getHimPenisWidth, this, []), value);
			if (isNaN(newVal)) {
				m.displayMessageRed("da.him.body.penis.width attempted set to " + value + ", unable to identify value as numeric");
				return;
			}
			g.him.loadPenisScales(g.him.currentPenisLengthScale, newVal);
		}
		
		public function getHimPenisLength(...args):Number {
			return g.him.penis.scaleX;
		}
		
		public function setHimPenisLength(value:*):void {
			var newVal:Number = calculateNewValue(new FunctionObject(getHimPenisLength, this, []), value);
			if (isNaN(newVal)) {
				m.displayMessageRed("da.him.body.penis.length attempted set to " + value + ", unable to identify value as numeric");
				return;
			}
			g.him.loadPenisScales(newVal, g.him.currentPenisWidthScale);
		}
		
		public function getHimBallsType(...args):String {
			return g.him.ballsNameArray[g.him.currentBalls];
		}
		
		public function setHimBallsType(value:*):void {
			var ballIndex:int = g.him.ballsNameArray.indexOf(value);
			if (ballIndex == -1) {
				m.displayMessageRed("da.him.body.balls attempted set to " + value + ", unable to find balltype");
				return;
			}
			g.him.setBalls(ballIndex);
		}
		
		public function getHimBreastSize(...args):Number {
			return g.him.currentBody.hasOwnProperty("breastSize") ? g.him.currentBody.breastSize : 0;
		}
		
		public function setHimBreastSize(value:*):void {
			var newVal:Number = calculateNewValue(new FunctionObject(getHimBreastSize, this, []), value);
			if (isNaN(newVal)) {
				m.displayMessageRed("da.him.body.breasts attempted set to " + value + ", unable to identify value as numeric");
				return;
			}
			
			if (g.him.currentBody.hasOwnProperty("breastSize")) { //if has breasts e.g. is Female (but also allow modded bodies which have breasts ...?)
				newVal = Math.min(newVal, 149);
				newVal = Math.max(newVal, 0);
				g.him.currentBody.setBreasts(newVal);
			}
		}
		
		public function getHimBallsize(...args):Number {
			return g.him.currentBallSize;
		}
		
		public function setHimBallsize(value:*):void {
			var newVal:Number = calculateNewValue(new FunctionObject(getHimBallsize, this, []), value);
			if (isNaN(newVal)) {
				m.displayMessageRed("da.him.body.balls.size attempted set to " + value + ", unable to identify value as numeric");
				return;
			}
			g.him.setBallScale(newVal);
		}
		
		private function calculateNewValue(getter:FunctionObject, newValue:*):Number {
			var newVal:Number = 0;
			if (newValue is Number) {
				newVal = newValue;
			} else if (newValue is String && !isNaN(Number(newValue))) {
				if ((newValue as String).charAt(0) == "-" || (newValue as String).charAt(0) == "+" ) {
					newVal = getter.call() + Number(newValue);
				} else {
					newVal = Number(newValue);
				}
			} else {
				return NaN;
			}
			return newVal;
		}
		
		public function getHimGender(...args):String {
			return g.him.bodyNameList[g.him.currentBodyID];
		}
		
		public function setHimGender(value:*):void {
			var genderIndex:int = g.him.bodyNameList.indexOf(value);
			if (genderIndex == -1) {
				m.displayMessageRed("da.him.body.gender attempted set to " + value + ", unable to find body");
				return;
			}
			g.him.setBody(genderIndex);
		}
		
		public function getHimSkinType(...args):String {
			return g.him.currentBody.skinNameList[g.him.currentBody.currentSkinID];
		}
		
		public function setHimSkinType(value:*):void {
			var skinIndex:int = g.him.currentBody.skinNameList.indexOf(value);
			if (skinIndex == -1) {
				m.displayMessageRed("da.him.body.skin attempted set to " + value + ", unable to find skin");
				return;
			}
			var gender:String = getHimGender();
			if (gender == "Male") {
				g.him.currentBody.setSkin(skinIndex);
			} else if (gender == "Female") {
				g.him.currentBody.setSkinx(skinIndex); //kona pls why do you have such a poor class inheritance tree for the male body
			} else { 
				m.displayMessageRed("da.him.body.skin can't be set, gender is " + gender + " and DA does not recognize this");
				//https://www.undertow.club/threads/dialogueactions-wants-to-be-able-to-detect-other-him-bodies-than-male-female-but-how.11117/
				//summary: Kona wrote male/female bodies badly, and I have no idea if...
				//- modders even make custom bodies or just do something wonky with male clothing
				//- how they handle setSkin
			}
		}
		
		public function isBlockingOrgasmDialogue(...args):uint {
			return isBlockingOrgasm(args) ? 1 : 0;
		}
		
		public function isBlockingOrgasm(...args):Boolean {
			return blockingOrgasm;
		}
		
		public function setBlockingOrgasm(value:*):void {
			if (value is Number) {
				if (value == 1) {
					enableCumBlock();
				} else if (value == 0) {
					disableCumBlock();
				} else {
					m.displayMessageRed("Unrecognized value (" + value + ") for da.blockingOrgasm");
				}
			} else {
				m.displayMessageRed("Unrecognized value (" + value + ") for da.blockingOrgasm");
			}
		}
		
		public function isEjaculating(...args):Boolean {
			return g.him.ejaculating;
		}
		
		public function hideHim(...args):void {
			g.him.visible = false;
			g.him.armContainer.visible = false;
			g.him.overLayer.visible = false;
		}
		public function showHim(...args):void {
			g.him.visible = true;
			g.him.armContainer.visible = true;
			g.him.overLayer.visible = true;
		}
		
		public function hideHisArm(...args):void {
			if (g.him.visible) {
				g.him.armContainer.visible = false;
			}
		}
		public function showHisArm(...args):void {
			if (g.him.visible) {
				g.him.armContainer.visible = true;
			}
		}
		public function cum(...args):void {
			g.him.pleasure = g.him.ejacPleasure * 0.95;
			m.mc.addEventListener(Event.ENTER_FRAME, ejaculate);
		}
		
		public function enableCumBlock(...args):void {
			blockingOrgasm = true;
		}
		
		public function disableCumBlock(...args):void {
			blockingOrgasm = false;
		}
		
		public function ejaculate(e) {
			if (!g.him.ejaculating) {
				g.him.pleasure = g.him.pleasure + Math.max(((g.him.ejacPleasure * 0.01) / 30) + 0.08, 0.06);
			} else {
				m.mc.removeEventListener(Event.ENTER_FRAME, ejaculate);
			}
		}
		
	}

}
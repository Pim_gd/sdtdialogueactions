package  
{
	import flash.events.Event;
	
	public class RecordingTriggers extends CustomTriggerHolder
	{
		private var recPaused:Boolean;
		private var holdDown:Boolean;
		public function RecordingTriggers(G:Object, M:Main) 
		{
			super(G, M);
			recPaused = false;
			holdDown = false;
		}
		
		public function registerTriggers(t:TriggerManager):void {
			t.registerTrigger("AUTO_KEYS", 0, new FunctionObject(invertAutoKeys, this, []));
			t.registerTrigger("AUTO_KEYS_ON", 0, new FunctionObject(autoKeysOn, this, []));
			t.registerTrigger("AUTO_KEYS_OFF", 0, new FunctionObject(autoKeysOff, this, []));
			t.registerTrigger("PAUSE", 0, new FunctionObject(pauseRecording, this, []));
			t.registerTrigger("CONTINUE", 0, new FunctionObject(continueRecording, this, []));
			t.registerTrigger("PULL_OFF", 0, new FunctionObject(pullOff, this, []));
		}
		
		public function continueRecording(...args):void {
			var main:* = m.getLoaderRef();
			if (main.aCon && !main.playRec) {
				main.playRec = true;
				recPaused = false;
			}
		}
		
		public function pauseRecording(...args):void {
			var main:* = m.getLoaderRef();
			if (main.aCon && main.playRec) {
				main.playRec = false;
				main.loc = main.loadedSWF.currentMousePos.x;
				recPaused = true;
			}
		}
		
		public function invertAutoKeys(...args):void {
			var main:* = m.getLoaderRef();
			main.aCon = !main.aCon;
		}
		
		public function autoKeysOn(...args):void {
			m.getLoaderRef().aCon = true;
		}
		
		public function autoKeysOff(...args):void {
			m.getLoaderRef().aCon = false;
		}
		
		public function pullOff(...args):void {
			holdDown = m.getLoaderRef().holdDown;
			m.mc.addEventListener(Event.ENTER_FRAME, checkPull);
		}
		
		public function checkPull(e:Event = null):void {
			if (Math.abs(g.her.releasedPos - g.her.pos) > 0.1) {
				m.getLoaderRef().holdDown = false;
				var acceleration:Number = 0;
				if (g.her.released) {
					acceleration = (g.her.releasedPos - g.her.pos) / g.her.moveSmoothing;
				} else {
					acceleration = (g.her.releasedPos - g.her.pos) / 5;
				}
				g.her.pos = Math.max(g.her.minPos, Math.min(g.her.maxPos, g.her.pos + acceleration));
			} else {
				pauseRecording();
				m.getLoaderRef().holdDown = holdDown;
				m.mc.removeEventListener(Event.ENTER_FRAME, checkPull);
			}
		}
	}

}
package  
{
	import flash.net.SharedObject;
	import flash.net.SharedObjectFlushStatus;
	/**
	 * ...
	 * @author Pimgd
	 */
	public class VariableTriggers extends CustomTriggerHolder
	{
		private var VASave:SharedObject;
		public function VariableTriggers(G:Object,M:Main) 
		{
			super(G, M);
		}
		
		public function registerTriggers(t:TriggerManager):void {
			t.registerTrigger("APPENDVAR", 2, new FunctionObject(appendVariable, this, []));
			t.registerTrigger("APPENDVARBYNAME", 2, new FunctionObject(appendVariableByName, this, []));
			t.registerTrigger("APPENDVARBYNAEM", 2, new FunctionObject(appendVariableByName, this, []));//Alias for preventing SDT's retarded ME replacement
			t.registerTrigger("SETVAR", 2, new FunctionObject(setVariable, this, []));
			t.registerTrigger("SETVARBYNAME", 2, new FunctionObject(setVariableByName, this, []));
			t.registerTrigger("SETVARBYNAEM", 2, new FunctionObject(setVariableByName, this, []));//Alias for preventing SDT's retarded ME replacement
			t.registerTrigger("SETGLOBAL", 2, new FunctionObject(setGlobalVariable, this, []));
			t.registerTrigger("SETGLOBALBYNAME", 2, new FunctionObject(setGlobalVariableByName, this, []));
			t.registerTrigger("SETGLOBALBYNAEM", 2, new FunctionObject(setGlobalVariableByName, this, []));//Alias for preventing SDT's retarded ME replacement
			//x[COPYOBJECT_layers_variable_otherVariable] This ones's new. I'll explain it later.
			//x[COPYOBJECT_layers_variable_otherVariable_USING_structInfoVariable] Also new.
			//x[REGISTERGLOBALS_variable1_variable2_variableN] Pushes ALL of the variables listed into global space. Supports objects.
			//x[UNREGISTERGLOBALS_variable1_variable2_variableN] Decouples ALL of the variables listed from global space. Supports objects.
			//x[DELETEGLOBALS_variable1_variable2_variableN] Deletes ALL of the variables listed from global space. Supports objects.
			//[DELETEVARS_variable1_variable2_variableN] Deletes ALL of the variables listed from local space. Supports objects.
			//x[LOADGLOBALS_variable1_variable2_variableN] Loads all of the listed variables from global space into your dialogue. Links them automatically. Supports objects.
			//x[GETGLOBALS_variable1_variable2_variableN] Loads all of the listed variables from global space into your dialogue, unlinked. Existing links are not erased. Supports objects.
			//x[SETGLOBALS_variable1_variable2_variableN] Sets all of the listed variables from local space into the global space, unlinked. Existing links are not erased.
			//x[SAVESETVARS_name_variable1_variable2_variableN] Creates a savefile with name if not exists and adds the variables to the savefile. Supports objects.
			//x[SAVEGETVARS_name_variable1_variable2_variableN] Loads from a savefile with the supplied name the listed variables to the savefile. Supports objects.
			//[SAVEREMOVEVARS_name_variable1_variable2_variableN] Removes variables from a savefile. Supports objects.
			//x[SAVEDELETE_name] Deletes a savefile.
			
			t.registerTrigger("REGISTERGLOBALS", -1, new FunctionObject(registerGlobalVariables, this, []));
			t.registerTrigger("UNREGISTERGLOBALS", -1, new FunctionObject(unregisterGlobalVariables, this, []));
			t.registerTrigger("DELETEGLOBALS", -1, new FunctionObject(deleteGlobalVariables, this, []));
			t.registerTrigger("DELETELOCALS", -1, new FunctionObject(deleteLocalVariables, this, []));
			t.registerTrigger("SETGLOBALS", -1, new FunctionObject(copyLocalVariablesToGlobal, this, []));
			t.registerTrigger("GETGLOBALS", -1, new FunctionObject(copyGlobalVariablesToLocal, this, []));
			t.registerTrigger("LOADGLOBALS", -1, new FunctionObject(loadGlobalVariables, this, []));
			t.registerTrigger("COPYOBJECT", 3, new FunctionObject(copyVariablesObject, this, []));
			t.registerTrigger("COPYOBJECT", 5, new FunctionObject(copyVariablesObjectUsingStructInfo, this, []));//USING
			t.registerTrigger("DEFINEOBJECT", -1, new FunctionObject(defineObject, this, []));//Creates variables
			t.registerTrigger("SAVESETVARS", -1, new FunctionObject(saveVariablesToSharedObject, this, []));
			t.registerTrigger("SAVEGETVARS", -1, new FunctionObject(loadVariablesFromSharedObject, this, []));
			t.registerTrigger("SAVEREMOVEVARS", -1, new FunctionObject(removeVariablesFromSharedObject, this, []));
			t.registerTrigger("SAVEDELETE", 1, new FunctionObject(deleteSavefile, this, []));
			t.registerTrigger("SPLITSTRING", 2, new FunctionObject(splitStringLiteral, this, []));
			t.registerTrigger("SPLITSTRINGBYNAME", 2, new FunctionObject(splitStringVariable, this, []));
			t.registerTrigger("SPLITSTRINGBYNAEM", 2, new FunctionObject(splitStringVariable, this, []));
		}
		
		public function defineObject(... args):void {
			if (args[0].length <= 1) {
				return;
			}
			var rootVariableName:String = args[0][0];
			for (var i:uint = 1; i < args[0].length; i++) {
				m.getVariableManager().setVariableValueViaSDT(rootVariableName + args[0][i], 0);
			}
		}
		
		public function copyVariablesObject(... args):void {
			copyVariablesObjectI(args[0][0], args[0][1], args[0][2]);
		}
		
		public function copyVariablesObjectI(layers:String, targetObjectVariable:String, sourceObjectVariable:String):void {
			var layerCount:Number = Number(layers);
			if (!isNaN(layerCount)) {
				m.getVariableManager().copyLocalObject(layerCount, targetObjectVariable, sourceObjectVariable);
			}
		}
		
		public function copyVariablesObjectUsingStructInfo(... args):void {
			copyVariablesObjectUsingStructInfoI(args[0][0], args[0][1], args[0][2], args[0][3], args[0][4]);
		}
		
		public function copyVariablesObjectUsingStructInfoI(layers:String, targetObjectVariable:String, sourceObjectVariable:String, using:String, structInfoObjectVariable:String):void {
			if (using != "USING") {
				return;//wat happened
			}
			var layerCount:Number = Number(layers);
			if (!isNaN(layerCount)) {
				m.getVariableManager().copyLocalObjectUsingStructInfo(layerCount, targetObjectVariable, sourceObjectVariable, structInfoObjectVariable);
			}
		}
		
		
		
		private function isObjectVariable(variableName:String):Boolean {
			return (variableName != "" && variableName.charAt(variableName.length - 1) == ".");//if not empty and has period at end
		}
		
		public function registerGlobalVariables(... args):void {
			for (var i:uint = 0; i < args[0].length; i++) {
				var variableName:String = args[0][i];
				if (isObjectVariable(variableName)) {
					m.getVariableManager().registerGlobalObjectVariable(variableName);
				} else {
					m.getVariableManager().registerGlobalVariable(variableName);
				}
			}
		}
		
		public function unregisterGlobalVariables(... args):void {
			for (var i:uint = 0; i < args[0].length; i++) {
				var variableName:String = args[0][i];
				if (isObjectVariable(variableName)) {
					m.getVariableManager().unlinkGlobalObjectVariable(variableName);
				} else {
					m.getVariableManager().unlinkGlobalVariable(variableName);
				}
			}
		}
		
		public function deleteGlobalVariables(... args):void {
			for (var i:uint = 0; i < args[0].length; i++) {
				var variableName:String = args[0][i];
				if (isObjectVariable(variableName)) {
					m.getVariableManager().deleteGlobalObjectVariable(variableName);
				} else {
					m.getVariableManager().deleteGlobalVariable(variableName);
				}
			}
		}
		
		public function deleteLocalVariables(... args):void {
			for (var i:uint = 0; i < args[0].length; i++) {
				var variableName:String = args[0][i];
				if (isObjectVariable(variableName)) {
					m.getVariableManager().deleteLocalObjectVariable(variableName);
				} else {
					m.getVariableManager().deleteLocalVariable(variableName);
				}
			}
		}
		
		public function copyLocalVariablesToGlobal(... args):void {
			for (var i:uint = 0; i < args[0].length; i++) {
				var variableName:String = args[0][i];
				if (isObjectVariable(variableName)) {
					m.getVariableManager().copyLocalVariableToGlobalObject(variableName);
				} else {
					m.getVariableManager().copyLocalVariableToGlobal(variableName);
				}
			}
		}
		
		public function copyGlobalVariablesToLocal(... args):void {
			for (var i:uint = 0; i < args[0].length; i++) {
				var variableName:String = args[0][i];
				if (isObjectVariable(variableName)) {
					m.getVariableManager().copyGlobalVariableToLocalObject(variableName);
				} else {
					m.getVariableManager().copyGlobalVariableToLocal(variableName);
				}
			}
		}
		
		public function loadGlobalVariables(... args):void {
			for (var i:uint = 0; i < args[0].length; i++) {
				var variableName:String = args[0][i];
				if (isObjectVariable(variableName)) {
					m.getVariableManager().loadGlobalVariableObject(variableName);
				} else {
					m.getVariableManager().loadGlobalVariable(variableName);
				}
			}
		}
		
		
		
		public function setGlobalVariable(... args):void {
			//m.displayMessageRed("VA_SET_GLOBALVARIABLE is deprecated!");
			if (args[0] is Array) {
				setGlobalVariable(args[0][0], args[0][1]);
			} else {
				m.getVariableManager().setGlobalVariableValue(args[0], args[1]);
			}
		}
		
		public function setGlobalVariableByName(... args):void {
			if (args[0] is Array) {
				setGlobalVariableByName(args[0][0], args[0][1]);
			} else {
				m.getVariableManager().setGlobalVariableValue(args[0], m.getVariableManager().getVariableValueViaSDT(args[1]));
			}
		}
		
		public function appendVariable(... args):void {
			if (args[0] is Array) {
				appendVariable(args[0][0], args[0][1]);
			} else {
				appendVariableI(args[0], args[1]);
			}
		}
		
		public function appendVariableI(name:String, value:String):void {
			var mainValue:* = m.getVariableManager().getVariableValueViaSDT(name);
			var newValue:* = mainValue + value;
			m.getVariableManager().setVariableValueViaSDT(name, isNaN(Number(newValue)) ? newValue : Number(newValue));
			
		}
		
		public function appendVariableByName(... args):void {
			if (args[0] is Array) {
				appendVariableByName(args[0][0], args[0][1]);
			} else {
				appendVariableByNameI(args[0], args[1]);
			}
		}
		
		public function appendVariableByNameI(name:String, value:String):void {
			var mainValue:* = m.getVariableManager().getVariableValueViaSDT(name);
			var secondValue:String = m.getVariableManager().getVariableValueViaSDT(value);
			var newValue:* = mainValue + secondValue;
			m.getVariableManager().setVariableValueViaSDT(name, isNaN(Number(newValue)) ? newValue : Number(newValue));
			
		}

		public function setVariable(... args):void {
			if (args[0] is Array) {
				setVariable(args[0][0], args[0][1]);
			} else {
				setVariableI(args[0], args[1]);
			}
		}
		
		public function setVariableI(name:String, value:String):void {
			var val:Number;
			
			if (value.indexOf("+=") == 0) {
				val = Number(value.substr(2));
				if (isNaN(val)) {
					m.displayMessageRed("Failed to identify numerical value in trigger [SETVAR_" + name + "_" + value + "]");
					return;
				} else {
					if (val < 0) {
						m.getVariableManager().setVariableValueViaSDT(name, "" + val);
					} else {
						m.getVariableManager().setVariableValueViaSDT(name, "+" + val);
					}
				}
				
			} else if (value.indexOf("-=") == 0) {
				val = Number(value.substr(2));
				if (isNaN(val)) {
					m.displayMessageRed("Failed to identify numerical value in trigger [SETVAR_" + name + "_" + value + "]");
					return;
				} else {
					val = val * -1;
					if (val < 0) {
						m.getVariableManager().setVariableValueViaSDT(name, "" + val);
					} else {
						m.getVariableManager().setVariableValueViaSDT(name, "+" + val);
					}
				}
			} else {
				val = Number(value);
				if (isNaN(val)) {
					m.getVariableManager().setVariableValueViaSDT(name, value);
				} else {
					m.getVariableManager().setVariableValueViaSDT(name, val);
				}
			}
		}
		
		public function setVariableByName(... args):void {
			if (args[0] is Array) {
				setVariableByName(args[0][0], args[0][1]);
			} else {
				setVariableByNameI(args[0], args[1]);
			}
		}
		
		public function setVariableByNameI(name:String, value:String):void {
			var varValue:String = m.getVariableManager().getVariableValueViaSDT(value);
			var varVal:Number = Number(varValue);

			if (isNaN(varVal)) {
				m.getVariableManager().setVariableValueViaSDT(name, varValue);
			} else {
				m.getVariableManager().setVariableValueViaSDT(name, varVal);
			}
		
		}
		
		public function splitStringInto(source:String, targetObjectVariableName:String):void {
			if (!StringFunctions.stringEndsWith(targetObjectVariableName, ".")) { 
				targetObjectVariableName += ".";
			}
			var result:Array = source.split("");
			m.getVariableManager().setVariableValueViaSDT(targetObjectVariableName + "length", result.length);
			for (var i:uint = 0, isize:uint = result.length; i < isize; i++) { 
				m.getVariableManager().setVariableValueViaSDT(targetObjectVariableName + "charAt." + i, result[i]);
			}
		}
		
		public function splitStringLiteral(...args):void {
			var stringLiteral:String = args[0][0];
			var targetObjectVariable:String = args[0][1];
			splitStringInto(stringLiteral, targetObjectVariable);
		}
		
		public function splitStringVariable(...args):void {
			var stringVariableName:String = args[0][0];
			var targetObjectVariable:String = args[0][1];
			var stringLiteral:String = "" + m.getVariableManager().getVariableValueViaSDT(stringVariableName);
			splitStringInto(stringLiteral, targetObjectVariable);
		}
		
		public function saveVariablesToSharedObject(...args):void {
			if (args[0].length < 2) {
				return;//I can't save if you don't give me any variables!
			}
			var saveFileName:String = args[0][0];
			m.displayMessageGreen("Saving to " + saveFileName);
			var saveFile:SharedObject = SharedObject.getLocal(saveFileName);
			for (var i:uint = 1; i < args[0].length; i++) {
				var variableName:String = args[0][i];
				if (isObjectVariable(variableName)) {
					var variables:Array = m.getVariableManager().getLocalVariablesDenotedByObjectNotation(variableName);
					for (var j:uint = 0; j < variables.length; j++) {
						saveVariableToSharedObject(saveFile, variables[j]);
					}
				} else {
					saveVariableToSharedObject(saveFile, variableName);
				}
			}
			if (saveFile.flush() == SharedObjectFlushStatus.PENDING) {
				m.displayMessageRed("Saving failed: Not enough space alloted to Flash");
			}
		}
		
		public function loadVariablesFromSharedObject(...args):void {
			if (args[0].length < 2) {
				m.displayMessageRed("loading failed: not enough args");
				return;//I can't load if you don't give me any variables!
			}
			var saveFileName:String = args[0][0];
			m.displayMessageGreen("loading from " + saveFileName);
			var saveFile:SharedObject = SharedObject.getLocal(saveFileName);
			for (var i:uint = 1; i < args[0].length; i++) {
				loadVariableFromSharedObject(saveFile, args[0][i]);
			}
		}
		
		private function loadVariableFromSharedObject(saveFile:SharedObject, variableName:String):void {
			if (isObjectVariable(variableName)) {
				//m.displayMessageWhite("loading object var " +variableName);
				for (var varName:String in saveFile.data) {
					if (varName.indexOf(variableName) == 0 && varName.length > variableName.length) {
						loadSimpleVariableFromSharedObject(saveFile, varName);
					}
				}
			} else if (saveFile.data[variableName] != undefined) {
				//m.displayMessageWhite("loading normal var " +variableName);
				loadSimpleVariableFromSharedObject(saveFile, variableName);
			}
		}
		
		/**
		 * Loads a simple variable from a shared object, assumes valid inputs
		 * @param	saveFile
		 * @param	variableName
		 */
		private function loadSimpleVariableFromSharedObject(saveFile:SharedObject, variableName:String):void {
			var variableManager:VariableManager = m.getVariableManager();
			variableManager.deleteLocalVariable(variableName);//safety measure to prevent + and -
			var val:Number = Number(saveFile.data[variableName]);
			if (isNaN(val)) {
				variableManager.setVariableValueViaSDT(variableName, saveFile.data[variableName]);
			} else {
				variableManager.setVariableValueViaSDT(variableName, val);
			}
		}
		
		public function removeVariablesFromSharedObject(...args):void {
			if (args[0].length < 2) {
				return;//I can't remove them if you don't give me any variables!
			}
			var saveFileName:String = args[0][0];
			var saveFile:SharedObject = SharedObject.getLocal(saveFileName);
			for (var i:uint = 1; i < args[0].length; i++) {
				removeVariableFromSharedObject(saveFile, args[0][i]);
			}
		}
		
		public function deleteSavefile(...args):void {
			var saveFileName:String = args[0][0];
			var saveFile:SharedObject = SharedObject.getLocal(saveFileName);
			saveFile.clear();
		}
		
		private function saveVariableToSharedObject(saveFile:SharedObject, variableName:String):void {
			saveFile.data[variableName] = m.getVariableManager().getVariableValueViaSDT(variableName);
		}
		
		private function removeVariableFromSharedObject(saveFile:SharedObject, variableName:String):void {
			if (isObjectVariable(variableName)) {
				for (var varName:String in saveFile.data) {
					if (varName.indexOf(variableName) == 0 && varName.length > variableName.length) {
						delete saveFile.data[varName];
					}
				}
			} else {
				delete saveFile.data[variableName];
			}
		}
		
	}

}
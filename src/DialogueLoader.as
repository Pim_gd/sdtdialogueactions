package 
{
	import flash.net.URLRequest;
	import flash.net.URLLoader;
	import flash.events.IOErrorEvent;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Pimgd
	 */
	public class DialogueLoader 
	{
		public static function loadDialogueFile(fullPath:String, successCallbackFunction:FunctionObject, errorCallbackFunction:FunctionObject):void {
			var dialogueLoader:URLLoader = new URLLoader();
			dialogueLoader.addEventListener(Event.COMPLETE, successCallbackFunction.call);
			var errorArgs:Array = errorCallbackFunction.getArgs();
			errorArgs.push(fullPath);
			errorCallbackFunction.setArgs(errorArgs);
			dialogueLoader.addEventListener(IOErrorEvent.IO_ERROR, errorCallbackFunction.call);
			
			dialogueLoader.load(new URLRequest(fullPath));
		}
	}

}
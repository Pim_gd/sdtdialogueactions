package {
	
	import flash.media.Sound;
	
	[Embed(source="../audio/WWSounds_AhHard04.swf",symbol="WWSound_AhHard04")]
	
	public dynamic class WWSound_AhHard04 extends Sound {
		
		public function WWSound_AhHard04() {
			super();
		}
	}

}

package {
	
	import flash.media.Sound;
	
	[Embed(source="../audio/WWSounds_OhSoft01_2.swf",symbol="WWSound_OhSoft01_2")]
	
	public dynamic class WWSound_OhSoft01_2 extends Sound {
		
		public function WWSound_OhSoft01_2() {
			super();
		}
	}

}

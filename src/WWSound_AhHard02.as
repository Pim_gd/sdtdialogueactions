package {
	
	import flash.media.Sound;
	
	[Embed(source="../audio/WWSounds_AhHard02.swf",symbol="WWSound_AhHard02")]
	
	public dynamic class WWSound_AhHard02 extends Sound {
		
		public function WWSound_AhHard02() {
			super();
		}
	}

}

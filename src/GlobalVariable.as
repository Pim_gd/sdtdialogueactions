package  
{
	/**
	 * Simple value class to stow in an array.
	 * @author Pimgd
	 */
	public class GlobalVariable 
	{
		private var value:*;
		private var attached:Boolean = false;
		public function GlobalVariable(Value:*, Attached:Boolean) 
		{
			value = Value;
			attached = Attached;
		}
		
		public function setValue(val:*) {
			value = val;
		}
		
		public function getValue():* {
			return value;
		}
		
		public function setAttached(attach:Boolean) {
			attached = attach;
		}
		
		public function isAttached():Boolean {
			return attached;
		}
		
	}

}
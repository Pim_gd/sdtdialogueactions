package  
{
	/**
	 * ...
	 * @author Pimgd
	 */
	public class TongueTriggers extends CustomTriggerHolder
	{
		public function TongueTriggers(G:Object, M:Main) 
		{
			super(G, M);
		}
		
		public function registerTriggers(t:TriggerManager):void {
			t.registerTrigger("TONGUE_IN", 0, new FunctionObject(tongueIn, this, []));
			t.registerTrigger("TONGUE_OUT", 0, new FunctionObject(tongueOut, this, []));
		}
		
		public function tongueIn(...args):void {
			g.her.tongue.stopHangingOut();
		}
		
		public function tongueOut(...args):void {
			g.her.tongue.hangOut();
		}
	}

}
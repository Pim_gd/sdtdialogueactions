package {
	
	import flash.media.Sound;
	
	[Embed(source="../audio/WWSounds_OhSoft05_2.swf",symbol="WWSound_OhSoft05_2")]
	
	public dynamic class WWSound_OhSoft05_2 extends Sound {
		
		public function WWSound_OhSoft05_2() {
			super();
		}
	}

}

package {
	
	import flash.media.Sound;
	
	[Embed(source="../audio/WWSounds_AhHard02_2.swf",symbol="WWSound_AhHard02_2")]
	
	public dynamic class WWSound_AhHard02_2 extends Sound {
		
		public function WWSound_AhHard02_2() {
			super();
		}
	}

}

package  
{
	/**
	 * ...
	 * @author Pimgd
	 */
	public class StringQuation 
	{
		
		public function StringQuation() 
		{
			
		}
		public static function equationMath(equationset:String):Number {
			//You know, this might be AS2... but it fits PERFECTLY in AS3. I hope.
			var possibleActions:Array = new Array("+", "-", "*", "%", "\\", "/", "!", "=", ">", "<", "|", "&");
			var possibleDoublechars:Array = new Array("!=", "==", ">=", "<=", "||", "&&");
			var possibleSinglechars:Array = new Array("+", "-", "*", "%", "\\", "/", "=", ">", "<");
			var digits:Array = new Array();
			var actions:Array = new Array();
			var equation:String;
			var currentpart:String = "";
			var endanswer = 0;
			var checkingParentheses:Boolean = false;
			var parenthesesCount:Number = 0;
			var parenthesesString:String = "";
			var parenthesesIndex:Number = 0;
			equation = equationset;
			for (var i:Number = 0; i < equation.length; i++) {
				if (equation.charAt(i) == "(") {
					parenthesesCount++;
					if (!checkingParentheses) {
						parenthesesIndex = i;
					}
					checkingParentheses = true;
				} else if (equation.charAt(i) == ")") {
					parenthesesCount--;
				}
				if (checkingParentheses) {
					if (parenthesesCount == 0) {
						equation = equation.substr(0, parenthesesIndex) + equationMath(parenthesesString.substr(1)) + equation.substr(parenthesesIndex + parenthesesString.length+1);
						parenthesesString = "";
						checkingParentheses = false;
						i = (parenthesesIndex-1);
					} else {
						parenthesesString += equation.charAt(i);
					}
				} else {
					if (equation.charAt(i) == " ") {
						//lololol skip
					} else if (currentpart.length > 0) {
						if (possibleActions.indexOf(equation.charAt(i)) == -1) {
							currentpart += equation.charAt(i);
						} else if (possibleActions.indexOf(equation.charAt(i)) < 3) {
							digits[digits.length] = currentpart;
							currentpart = "";
							actions[actions.length] = equation.charAt(i);
						} else if (possibleDoublechars.indexOf(equation.charAt(i)+equation.charAt(i+1)) != -1) {
							digits[digits.length] = currentpart;
							currentpart = "";
							actions[actions.length] = equation.charAt(i) + equation.charAt(i + 1);
							i++;
						} else if (possibleSinglechars.indexOf(equation.charAt(i)) != -1) {
							digits[digits.length] = currentpart;
							currentpart = "";
							actions[actions.length] = equation.charAt(i);
						}
					} else {
						currentpart += equation.charAt(i);
					}
					if (equation.length - 1 == i) {
						digits[digits.length] = currentpart;
					}
				}
			}
			endanswer = isNaN(Number(digits[0])) ? digits[0] : Number(digits[0]);
			//trace(endanswer);
			for (var i:Number = 0; i < actions.length; i++) {
				if (actions[i] == "+") {
					endanswer += isNaN(Number(digits[i+1])) ? digits[i+1] : Number(digits[i+1]);//allows String concatenation... nasty, but it works.
				} else if (actions[i] == "-") {
					endanswer -= Number(digits[i + 1]);
				} else if (actions[i] == "*") {
					endanswer *= Number(digits[i + 1]);
					//trace(endanswer);
				} else if (actions[i] == "%") {
					endanswer = endanswer % Number(digits[i+1]);
				} else if (actions[i] == "/") {
					endanswer /= Number(digits[i + 1]);
				} else if (actions[i] == "\\") {
					endanswer = Math.floor(endanswer / Number(digits[i+1]));
				} else if (actions[i] == "!=") {
					endanswer = (endanswer != digits[i + 1] ? 1 : 0);
				} else if (actions[i] == "==") {
					//trace(endanswer);
					//trace(digits[i + 1]);
					endanswer = (endanswer == digits[i + 1] ? 1 : 0);
				} else if (actions[i] == ">=") {
					endanswer = (endanswer >= Number(digits[i + 1]) ? 1 : 0);
				} else if (actions[i] == "<=") {
					endanswer = (endanswer <= Number(digits[i + 1]) ? 1 : 0);
				} else if (actions[i] == "<") {
					endanswer = (endanswer < Number(digits[i + 1]) ? 1 : 0);
				} else if (actions[i] == ">") {
					endanswer = (endanswer > Number(digits[i + 1]) ? 1 : 0);
				} else if (actions[i] == "=") {
					endanswer = digits[i + 1];
				} else if (actions[i] == "||") {
					endanswer = ((endanswer + Number(digits[i + 1])>=1) ? 1 : 0);
				} else if (actions[i] == "&&") {
					endanswer = ((endanswer + Number(digits[i + 1])==2) ? 1 : 0);
				}
			}
			//trace(Number(endanswer));
			return isNaN(Number(endanswer)) ? endanswer : Number(endanswer);
		}
	}

}
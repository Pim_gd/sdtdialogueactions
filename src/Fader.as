package  
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.utils.getTimer;
	/**
	 * ...
	 * @author Pimgd
	 */
	public class Fader extends Sprite
	{
		private var functionObject:FunctionObject;
		private var fadeDelay:int;
		private var hexColor:String;
		private var previousFrame:int = 0;
		private var triggerObject:FunctionObject;
		public function Fader() 
		{
			//this.addEventListener(Event.ADDED_TO_STAGE, startFade);
			fadeDelay = 0;
			hexColor = "000000";
		}
		
		/*public function startFade() {
			this.addEventListener(Event.REMOVED_FROM_STAGE, endFade);
		}
		
		public function fadeIn() {
			
		}*/
		
		public function setHexColor(hex:String):void {
			hexColor = hex;
			if (functionObject != null) {
				updateGraphics();
			}
		}
		
		public function setTriggerObject(f:FunctionObject):void {
			triggerObject = f;
		}
		
		private function updateGraphics():void {
			this.graphics.clear();
			this.graphics.beginFill(parseInt("0x"+hexColor));
			this.graphics.drawRect(0,0,700,600);
			this.graphics.endFill();
		}
		
		
		public function fade(hex:String):void {
			setHexColor(hex);
			if (functionObject != null) {
				this.removeEventListener(Event.ENTER_FRAME, functionObject.call);
			}
			functionObject = new FunctionObject(fadeIn, this, null);
			this.addEventListener(Event.ENTER_FRAME, functionObject.call);
			updateGraphics();
			this.alpha = 0;
			previousFrame = getTimer();
		}
		public function reduceFadeDelay():void {
			if (fadeDelay == -1) {
				return;
			}
			if (previousFrame == 0) {
				previousFrame = getTimer();
			}
			var diff:int = getTimer() - previousFrame;
			previousFrame = getTimer();
			fadeDelay -= diff;
			fadeDelay = Math.max(fadeDelay, 0);
		}
		public function fadeIn(e:Event = null):void {
			this.alpha = Math.min(this.alpha+0.02, 1);
			if (this.alpha >= 0.99) {
				if (fadeDelay != -1) {
					reduceFadeDelay();
					if (fadeDelay == 0) {
						endFade();
					}
				}
			}
		}
		public function fadeOut(e:Event = null):void {
			this.alpha = Math.max(this.alpha-0.02, 0);
			if (this.alpha <= 0.01) {
				instantEndFade();
			}
		}
		
		public function instantEndFade():void {
			this.removeEventListener(Event.ENTER_FRAME, functionObject.call);
			this.alpha = 0;
			this.graphics.clear();
			functionObject = null;
		}
		
		
		public function endFade():void {
			fadeDelay = 0;
			if (triggerObject != null) {
				triggerObject.call();
				triggerObject = null;
			}
			if (functionObject != null) {
				this.removeEventListener(Event.ENTER_FRAME, functionObject.call);
			}
			functionObject = new FunctionObject(fadeOut, this, null);
			this.addEventListener(Event.ENTER_FRAME, functionObject.call);
		}
		
		public function cleanUp():void {
			if (functionObject != null) {
				this.removeEventListener(Event.ENTER_FRAME, functionObject.call);
			}
			this.graphics.clear();
		}
		
		
		public function setFadeDelay(delay:int):void {
			fadeDelay = delay;
		}
	}

}
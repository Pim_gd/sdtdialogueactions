package {
	
	import flash.media.Sound;
	
	[Embed(source="../audio/WWSounds_OhHard04_2.swf",symbol="WWSound_OhHard04_2")]
	
	public dynamic class WWSound_OhHard04_2 extends Sound {
		
		public function WWSound_OhHard04_2() {
			super();
		}
	}

}

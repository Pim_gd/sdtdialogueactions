package  
{
	import flash.utils.Dictionary;
	import flash.utils.Proxy;
	import flash.utils.flash_proxy;
	/**
	 * Broken. Do not use.
	 * @author Pimgd
	 */
	public dynamic class DictionaryProxy extends Proxy
	{
		private var _dict:Dictionary;
		private var _m:Main;
		public function DictionaryProxy(m:Main, dict:Dictionary = null) 
		{
			if (dict == null) {
				_dict = new Dictionary();
			} else {
				_dict = dict;
			}
			_m = m;
		}
		
		
		
		override flash_proxy function hasProperty(name:*):Boolean {
			//_m.displayMessageGreen("hasProperty(name:* = " + name + "):Boolean = " + _dict.hasOwnProperty(name));
			return _dict.hasOwnProperty(name);
		}
		
		override flash_proxy function getProperty(name:*):* {
			//_m.displayMessageGreen("getProperty(name:* = " + name + "):* = " +  _dict[name]);
			return _dict[name];
		}

		override flash_proxy function setProperty(name:*, value:*):void {
			//_m.displayMessageGreen("setProperty(name:* = " + name + ", value:* = "+value+"):void");
			_dict[name] = value;
		}
	}

}
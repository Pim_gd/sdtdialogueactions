package {
	
	import flash.media.Sound;
	
	[Embed(source="../audio/WWSounds_OhHard05.swf",symbol="WWSound_OhHard05")]
	
	public dynamic class WWSound_OhHard05 extends Sound {
		
		public function WWSound_OhHard05() {
			super();
		}
	}

}

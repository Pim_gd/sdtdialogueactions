package  
{
	/**
	 * ehh
	 * I think I fucked up the structure
	 * and that I need CustomActionHolder
	 * and CustomTriggerHolder and CustomVariableHolder as interface
	 * Whatever, I can change that later.
	 * @author Pimgd
	 */
	public class HairActions extends CustomTriggerHolder
	{
		private var hairToLoad:String = "";
		private var hasLineListener:Boolean = false;
		public function HairActions(G:Object, M:Main) 
		{
			super(G, M);
		}
		
		public function registerTriggers(t:TriggerManager):void {
			t.registerTrigger("LOAD_HAIR", 0, new FunctionObject(loadHair, this, []));
		}
		
		public function registerVariables(v:VariableManager):void {
			var loadVarName:String = "da.hair.load";
			var fObject:FunctionObject = new FunctionObject(setHairFileName, this, []);
			v.registerVariableWrite(loadVarName, fObject);
		}
		
		public function setHairFileName(filename:String):void {
			hairToLoad = filename;
			m.addLineChangeListener(new FunctionObject(onLineChange, this, []));
			hasLineListener = true;
		}
		
		public function onLineChange(...args):void {
			if (g.dialogueControl.sayingPhrase.indexOf("[LOAD_HAIR]") == -1) {
				loadHairFile(hairToLoad);
			}
			hasLineListener = false;
		}
		
		public function loadHair(...args):void {
			if (hairToLoad != "") {
				loadHairFile(hairToLoad);
			} else if (!hasLineListener) {
				m.displayMessageRed("No hair set for loading. (Error: [LOAD_HAIR] without da.hair.load set)");
			}
		}
		
		public function loadHairFile(path:String):void {
			if (path.indexOf(".swf") != -1) {
				m.displayMessageRed("Don't load SWF hairs with da.hair.load! EXPLOSIONS IMMINENT");
			}
			//m.displayMessageGreen("loadHairFile - Mods/" + m.lastLoadedCData + "/" + path);
			g.customElementLoader.tryToLoadHairFile(m.getFileReferenceHandler().convertFilePath(path));
			hairToLoad = "";
		}
	}

}
package  
{
	/**
	 * ...
	 * @author Pimgd
	 */
	public class AutoTriggers extends CustomTriggerHolder
	{
		public function AutoTriggers(G:Object, M:Main) 
		{
			super(G, M);
		}
		
		public function registerTriggers(t:TriggerManager):void {
			t.registerTrigger("AUTO_OFF", 0, new FunctionObject(autoOff, this, []));
			t.registerTrigger("AUTO_SOFT", 0, new FunctionObject(autoSoft, this, []));
			t.registerTrigger("AUTO_NORMAL", 0, new FunctionObject(autoNormal, this, []));
			t.registerTrigger("AUTO_HARD", 0, new FunctionObject(autoHard, this, []));
			t.registerTrigger("AUTO_SELF", 0, new FunctionObject(autoSelf, this, []));
		}
		
		public function autoOff(...args):void {
			g.autoModeOn = false;
			updateAutoMenu();
			g.automaticControl.startAuto(g.currentPos);
		}
		
		public function autoNormal(...args):void {
			setAuto(0);
		}
		public function autoSoft(...args):void {
			setAuto(1);
		}
		public function autoHard(...args):void {
			setAuto(2);
		}
		public function autoSelf(...args):void {
			setAuto(3);
		}
		public function setAuto(mode:int):void {
			//g.autoMode = mode;
			g.setAuto(mode);//should enable everything and all
			g.autoModeOn = true;
			g.automaticControl.startAuto(g.currentPos);
			updateAutoMenu();
		}

		public function updateAutoMenu():void {
			g.inGameMenu.updateAutoList();
			g.inGameMenu.setCBAuto();
		}
	}

}
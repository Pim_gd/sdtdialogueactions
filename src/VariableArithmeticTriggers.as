package {
	// Added by WeeWillie 3/5/14
	import flash.net.SharedObject;
	
	/**
	 * ...
	 * @author Pimgd
	 */
	public class VariableArithmeticTriggers extends CustomTriggerHolder {
		private var variables:Array = new Array();
		// Added by WeeWillie 3/5/14
		private var VASave:SharedObject;
		
		public function VariableArithmeticTriggers(G:Object, M:Main) {
			super(G, M);
			variables = new Array();
		}
		
		public function registerTriggers(t:TriggerManager):void {
			t.registerTrigger("VA_SET_GLOBALVARIABLE", 2, new FunctionObject(setGlobalVariable, this, []));
			t.registerTrigger("VA_SET_GLOBALVARIABLEBYNAME", 2, new FunctionObject(setGlobalVariableByName, this, []));
			t.registerTrigger("VA_LOAD_GLOBALVARIABLE", 1, new FunctionObject(loadGlobalVariable, this, []));
			t.registerTrigger("VA_SET_VARIABLE", 2, new FunctionObject(setVariable, this, []));
			t.registerTrigger("VA_SET_VARIABLEBYNAME", 2, new FunctionObject(setVariableByName, this, []));
			// Added by WeeWillie 3/5/14
			t.registerTrigger("VA_SAVE_SAVEGAME", 1, new FunctionObject(this.saveSaveGame, this, []));
			t.registerTrigger("VA_LOAD_SAVEGAME", 1, new FunctionObject(this.loadSaveGame, this, []));
			t.registerTrigger("VA_CLEAR_SAVEGAME", 1, new FunctionObject(this.clearSaveGame, this, []));
		}
		
		public function registerVariables(v:VariableManager):void {
			v.registerVariableRead("da.random", new FunctionObject(randomValue, this, []));
		}
		
		public function randomValue(... args):Number {
			return Math.random();
		}
		
		public function setGlobalVariable(... args):void {
			//m.displayMessageRed("VA_SET_GLOBALVARIABLE is deprecated!");
			if (args[0] is Array) {
				setGlobalVariable(args[0][0], args[0][1]);
			} else {
				setGlobalVariableI(args[0], args[1]);
			}
		}
		
		public function setGlobalVariableI(name:String, value:String):void {
			variables[name] = value;
			// Added by Wee Willie, 8/27/2014, global variables always have a regular counterpart.
			setVariableI(name, variables[name]);
		}
		
		public function setGlobalVariableByName(... args):void {
			if (args[0] is Array) {
				setGlobalVariableByName(args[0][0], args[0][1]);
			} else {
				setGlobalVariableByNameI(args[0], args[1]);
			}
		}

		public function setGlobalVariableByNameI(name:String, value:String):void {
			variables[name] = m.getVariableManager().getVariableValueViaSDT(value);
			//don't save to a local variable; if you want to store an intercepted variable for later use (charcode? Dynamic dialogue loading?), doing so ruins it
			// Added by Wee Willie, 8/27/2014, global variables always have a regular counterpart.
			//setVariableI(name, variables[name]);
		}

		public function loadGlobalVariable(name:String):void {
			var value:String = variables[name];
			if (value != null) {
				var valueAsNumber:Number = Number(value);
				// Changed by Pim_gd (via Wee Willie copy and paste) 9/7/14, handle integer variables properly.
				if (isNaN(valueAsNumber)) {
					m.getVariableManager().setVariableValueViaSDT(name, variables[name]);
				} else {
					m.getVariableManager().setVariableValueViaSDT(name, valueAsNumber);
				}
			} else {
				m.displayMessageRed("No global variable " + name + " is known. (on trigger: [VA_LOAD_GLOBALVARIABLE_" + name + "]");
			}
		}

		public function setVariable(... args):void {
			if (args[0] is Array) {
				setVariable(args[0][0], args[0][1]);
			} else {
				setVariableI(args[0], args[1]);
			}
		}
		
		public function setVariableI(name:String, value:String):void {
			var val:Number;
			
			if (value.indexOf("+=") == 0) {
				val = Number(value.substr(2));
				if (isNaN(val)) {
					m.displayMessageRed("Failed to identify numerical value in trigger [VA_SET_VARIABLE_" + name + "_" + value + "]");
					return;
				} else {
					if (val < 0) {
						m.getVariableManager().setVariableValueViaSDT(name, val);
					} else {
						m.getVariableManager().setVariableValueViaSDT(name, "+" + val);
					}
				}
				
			} else if (value.indexOf("-=") == 0) {
				val = Number(value.substr(2));
				if (isNaN(val)) {
					m.displayMessageRed("Failed to identify numerical value in trigger [VA_SET_VARIABLE_" + name + "_" + value + "]");
					return;
				} else {
					val = val * -1;
					if (val < 0) {
						m.getVariableManager().setVariableValueViaSDT(name, val);
					} else {
						m.getVariableManager().setVariableValueViaSDT(name, "+" + val);
					}
				}
			} else {
				val = Number(value);
				if (isNaN(val)) {
					m.getVariableManager().setVariableValueViaSDT(name, value);
				} else {
					m.getVariableManager().setVariableValueViaSDT(name, val);
				}
			}
		}
		
		public function setVariableByName(... args):void {
			if (args[0] is Array) {
				setVariableByName(args[0][0], args[0][1]);
			} else {
				setVariableByNameI(args[0], args[1]);
			}
		}
		
		public function setVariableByNameI(name:String, value:String):void {
			var varValue:String = m.getVariableManager().getVariableValueViaSDT(value);
			var varVal:Number = Number(varValue);

			if (isNaN(varVal)) {
				m.getVariableManager().setVariableValueViaSDT(name, varValue);
			} else {
				m.getVariableManager().setVariableValueViaSDT(name, varVal);
			}
		
		}
		
		// Added by WeeWillie 3/3/14
		// Copy all the Global Variables into the save game name indicated by "saveName"
		public function saveSaveGame(saveName:String):void {
			var i:Number = 0;
			this.VASave = SharedObject.getLocal(saveName);
			m.displayMessageGreen("Saving Global to " + saveName);
			
			if (this.VASave.size == 0) {
				this.VASave.data.variables = new Array();
			}
			
			// Update and save each global variable out into the SharedObject
			for (var name:String in this.variables) {
				// Update this global variable from the associated dialogue variable.
				this.setGlobalVariableByNameI(name, name);
				
				// Save the global variable out to disk
				this.VASave.data.variables[name] = this.variables[name];
				i++;
			}
			m.displayMessageRed("Total Saved " + i);
			this.VASave.flush(1024);
		}
		
		// Added by WeeWillie 3/3/14
		// Set the Global Variables found in the save game specified by "saveName" to
		// the values in the save. 
		public function loadSaveGame(saveName:String):void {
			var i:Number = 0;
			this.VASave = SharedObject.getLocal(saveName);
			m.displayMessageGreen("Loading Global from " + saveName);
			
			if (this.VASave.size == 0) {
				this.VASave.data.variables = new Array();
			}
			
			// Load each global variable from the SharedObject
			for (var name:String in this.VASave.data.variables) {
				// Set the global variable from disk
				this.variables[name] = this.VASave.data.variables[name];
				
				// Load that global variable out to a usable dialog variable.
				setVariableI(name, this.variables[name]);
				i++;
			}
			m.displayMessageRed("Total Loaded " + i);
		}
		
		// Added by WeeWillie 3/3/14	  
		public function clearSaveGame(saveName:String):void {
			this.VASave = SharedObject.getLocal(saveName);
			if (this.VASave.size != 0) {
				this.VASave.clear();
				this.VASave.flush(1024);
			}
		}
	}

}
package {
	import flash.display.MovieClip;
	import flash.events.*;
	import flash.utils.Dictionary;
	import flash.utils.clearInterval;
	import flash.display.Loader;
	
	/**
	 * DialogueActions
	 * See changelog.txt for version information.
	 * @author Pimgd
	 */
	public dynamic class Main extends flash.display.MovieClip {
		private var main:*;
		private var g:*;
		private var onTriggerProxy:*;
		private var onDialogueVariablesResetProxy:*;
		public var lastLoadedCData:String;
		//private var onVariableInsertProxy:*;
		//private var onVariableWriteProxy:*;
		//private var onVariableCheckProxy:*;
		private var onDialogueLoadProxy:*;
		private var onLineChangeProxy:*;
		private var triggerManager:TriggerManager;
		private var variableManager:VariableManager;
		private var lineManager:LineTypeManager;
		private var lineChangeListeners:Array = new Array();
		public var mc:MovieClip = new MovieClip();
		
		private var currentLine:String = "";
		
		private var fileReferenceHandler:FileReferenceHandler;
		
		// Added WeeWillie 4/15/14
		public var modDataLoader:Class;
		//public var cData;
		
		// Button Declarations, added by WeeWillie 2/21/14
		public var wwButtonArray:Array = new Array(10);
		[Embed(source="../graphics/wwButton_btn.swf",symbol="wwButton_btn")]
		public var wwButton_btn:Class;
		
		private function getArrayKeysStartingWith(from:*, startingWith:String):Array {
			var variables:Array = new Array();
			for (var variableName:String in from) {
				if (variableName.indexOf(startingWith) == 0 && variableName.length > startingWith.length) {
					variables[variables.length] = variableName;
					//dialogueLog(variableName +" starts with " + startingWith);
				}
			}
			return variables;
		}
		
		public function Main() {
			//Empty body. Use for testing purposes.
			//var x:StringQuation = new StringQuation();
			//var y:Number = Math.random() * 10;
			//trace(x.equationMath("( aaa == aaa ) || ( aaa == aba )"));
			//trace(x.equationMath("aaa == aba"));
			//trace(y);
			//trace(3 - -2);
		/*var x = new Object();
		   x.dialogueControl = new Object();
		   x.dialogueControl.nextWord = function() { trace("nextword called"); };
		   x.dialogueControl.words = new Array();
		   x.dialogueControl.words[0] = new Object();
		   x.dialogueControl.words[0].action = "X_Y_Z_aaa";
		   x.dialogueControl.sayingWord = 0;
		   triggerManager = new TriggerManager(x);
		   triggerManager.registerTrigger("X_Y_Z", 1, new FunctionObject(doCheck, this, new Array()));
		 trace(onTrigger());*/
		}
		
		public function initl(l:*):void {
			//Loader init here.
			main = l; //loader object
			g = l.g; //game object
			
			triggerManager = new TriggerManager(g);
			variableManager = new VariableManager(g);
			lineManager = new LineTypeManager(g, this);
			fileReferenceHandler = new FileReferenceHandler(this);
			lastLoadedCData = main.cData;
			/*
			 * (3:39:03 PM) MG: well you could be super sneaky
			   (3:39:11 PM) MG: you know like the subloaders
			   (3:39:14 PM) MG: background loader etc
			   (3:39:25 PM) MG: place a listener on that for success/failure
			   (3:39:28 PM) MG: read out cData
			   (3:39:41 PM) MG: obgLdr.load(new URLRequest("Mods/" + cData + "/BG.png"));
			   (3:39:53 PM) MG: gets called right before $OVER$ gets loaded
			 */
			//dialogdict = g.dialogueControl.advancedController._dialogueDataStore;//That's how we get the dialog dict... but we shouldn't be using this.
			//Leave it for now.
			if (main.cData == "$INIT$") {
				main.addEnterFramePersist(doCheck);
			} else {
				main.addEnterFrame(doCheck);
			}
			addCustomActions();
			addProxies(main.cData == "$INIT$");
			addChild(mc);
			lineManager.onDialogueLoad();
			
			// Initialize the buttons, added by WeeWillie 2/21/14
			wwButtonInit();
			
			// Added by WeeWillie 3/5/14
			// Clear out the last custom dialogue on module load to
			// keep half-functional dialogs from loading without their needed assets.
			g.dialogueControl.resetCustomDialogue(false);
			
			// Register the event call every frame, added by WeeWillie 4/11/14
			main.registerUnloadFunction(doUnload);
			var apiDict:Dictionary = new Dictionary();
			apiDict["runTriggerCode"] = new FunctionObject(this.runTrigger, this, null);
			apiDict["registerTrigger"] = new FunctionObject(triggerManager.registerTriggerComms, triggerManager, null);
			apiDict["registerVariableWriteHandler"] = new FunctionObject(variableManager.registerVariableWriteHandlerComms, variableManager, null);
			apiDict["registerVariableReadHandler"] = new FunctionObject(variableManager.registerVariableReadHandlerComms, variableManager, null);
			apiDict["registerVariableWriteListener"] = new FunctionObject(variableManager.registerVariableWriteListenerComms, variableManager, null);
			apiDict["setVariableValue"] = new FunctionObject(variableManager.setVariableValueViaSDT, variableManager, null);
			apiDict["getVariableValue"] = new FunctionObject(variableManager.getVariableValueViaSDTRaw, variableManager, null);
			apiDict["addLineChangeListener"] = new FunctionObject(this.addLineChangeListenerComms, this, null);
			main.registerAPI("DialogueActions", apiDict, main.cData == "$INIT$");
			main.unloadMod(); //I have no idea what this is for. I think it's for declaring that you're done loading, and to add this to the scrapheap.
		}
		
		public function runTrigger(trigger:String):void {
			if (trigger != null) {
				try {
					if (!triggerManager.triggerFromStringAPI(trigger)) {
						g.dialogueControl.advancedController.outputLog("DialogueActions: runTriggerCode didn't recognize "+trigger);
					}
				} catch (e:Error) {
					displayMessageRed("CHECK DIALOGUE LOG - runTrigger caught Exception");
					g.dialogueControl.advancedController.outputLog("DialogueActions: runTriggerCode crashed on " + trigger);
					g.dialogueControl.advancedController.outputLog("DialogueActions: EXCEPTION ("+e.errorID+"): " + e.getStackTrace());
				}
			}
		}
		
		// Clean up when mod is unloaded.
		// Added by WeeWillie 4/11/14
		public function doUnload():void {
			main.clearDebug();
		}
		
		/**
		 * Eeeeewww undeclared variables
		 * @param	e
		 */
		public function doCheck(e = null):void {
			if (main.cData != lastLoadedCData) {
				if (main.cData != "$OVER$" && main.cData != "$RESET$") {
					lastLoadedCData = main.cData;
				}
			}
			if (!g.gamePaused) {
				lineManager.buildDialogueStates();
					//checkDialogueActions();
					//Nothing to do here! We're gonna use event based system
			}
		}
		
		private function addProxies(persist:Boolean):void {
			//trigger: g.dialogueControl.checkWordAction this.words[this.sayingWord].action
			//variable insert: g.dialogueControl.advancedController.customValueReplacer this._dialogueDataStore[arguments[1]]
			//variable write: g.dialogueControl.advancedController.setValues(param1 object)
			//variable read: g.dialogueControl.advancedController.canPlay (no loose read exist, reload all variables at function call)
			//dialogue load: g.dialogueControl.loadCustomDialogue - listener
			//line change: g.dialogueControl.startSpeakingPhrase - listener
			//cum blocker: g.him.move - listener
			var lProxy = main.lDOM.getDefinition("Modules.lProxy");
			var onTriggerProxy:Object = lProxy.createProxy(g.dialogueControl, "checkWordAction");
			onTriggerProxy.addPre(onTrigger, persist);
			//var onDialogueVariablesResetProxy = new lProxy(g.dialogueControl.advancedController, "reset", onDialogueVariablesReset, false);
			//onDialogueVariablesResetProxy.persists = persist;
			var onVariableInsertProxy:Object = lProxy.createProxy(g.dialogueControl.advancedController, "customValueReplacer");
			onVariableInsertProxy.addPre(onVariableInsert, persist);
			var onVariableWriteProxy:Object = lProxy.createProxy(g.dialogueControl.advancedController, "setValues");
			onVariableWriteProxy.addPre(onVariableWrite, persist);
			onVariableWriteProxy.hooked = false;
			var onVariableCheckProxy:Object = lProxy.createProxy(g.dialogueControl.advancedController, "canPlay");
			onVariableCheckProxy.addPre(proxyDialogueDict, persist);
			onVariableCheckProxy.addPost(unproxyDialogueDict, persist);
			
			var onLineChangeProxy:Object = lProxy.createProxy(g.dialogueControl, "startSpeakingPhrase");
			//onLineChangeProxy.persists = persist;
			//onLineChangeProxy.addPre(new FunctionObject(emptyFunction, this, []).call, persist);
			onLineChangeProxy.addPost(onLineChange, persist);
			var onSpeakingRandomPhraseProxy:Object = lProxy.createProxy(g.dialogueControl, "sayRandomPhrase");
			onSpeakingRandomPhraseProxy.addPre(unlockInstantLine, persist);
			onSpeakingRandomPhraseProxy.addPost(lockInstantLine, persist);
			var onDialogueLoadProxy:Object = lProxy.createProxy(g.dialogueControl, "loadCustomDialogue");
			//onDialogueLoadProxy.persists = persist;
			onDialogueLoadProxy.addPost(onDialogueLoad, persist);
			onDialogueLoadProxy.addPre(preDialogueLoad, persist);
			
			var onMoveProxy:Object = lProxy.createProxy(g.him, "move");
			onMoveProxy.addPost(onHimMove, persist);
		}
		
		private var proxiedVariables:Array = new Array();
		
		public function proxyDialogueDict(param1:*)
		{
			proxiedVariables = new Array();
			if (param1.settings["check"])
			{
				var variablesCheckedByLine:Object = param1.settings["check"];
				for (var variableName in variablesCheckedByLine)
				{
					if (!g.dialogueControl.advancedController._dialogueDataStore.hasOwnProperty(variableName))
					{
						var variableValueViaDA = variableManager.getVariableValue("" + variableName);
						if (variableValueViaDA != undefined)
						{
							g.dialogueControl.advancedController._dialogueDataStore[variableName] = variableValueViaDA;
							proxiedVariables.push(variableName);
						}
					}
				}
			}
		}
		
		public function unproxyDialogueDict(param1:*)
		{
			for each(var variableName in proxiedVariables)
			{
				delete g.dialogueControl.advancedController._dialogueDataStore[variableName];
			}
			proxiedVariables = new Array();
		}
		
		public function onHimMove(... args):void 
		{
			if (variableManager.getVariableValue(HimTriggers.ORGASM_BLOCK_VARIABLE_NAME)) {
				g.him.maxPleasureTimer = 0;
			}
		}
		
		public function emptyFunction(... args):void {
		
		}
		
		public function preDialogueLoad(... args):void {
			variableManager.unlinkAllGlobalVariables();
			variableManager.setVariableValue(HimTriggers.ORGASM_BLOCK_VARIABLE_NAME, 0);
		}
		
		private function addCustomActions():void {
			addCustomTriggers();
			addCustomVariables();
			var diag:DialogueVariables = new DialogueVariables(g, this);
			diag.registerVariables(variableManager);
			diag.registerTriggers(triggerManager);
			var hair:HairActions = new HairActions(g, this);
			hair.registerTriggers(triggerManager);
			hair.registerVariables(variableManager);
			var bg:BackgroundActions = new BackgroundActions(g, this);
			bg.registerTriggers(triggerManager);
			bg.registerVariables(variableManager);
			var charcode:CharCodeActions = new CharCodeActions(g, this);
			charcode.registerTriggers(triggerManager);
			charcode.registerVariables(variableManager);
			var her:HerTriggers = new HerTriggers(g, this);
			her.registerTriggers(triggerManager);
			her.registerVariables(variableManager);
			var variables:VariableTriggers = new VariableTriggers(g, this);
			variables.registerTriggers(triggerManager);
			var va:VariableArithmeticTriggers = new VariableArithmeticTriggers(g, this);
			va.registerTriggers(triggerManager);
			va.registerVariables(variableManager);//keep this when removing VA; da.random is added here
			var button:ButtonTriggers = new ButtonTriggers(g, this);
			button.registerTriggers(triggerManager);
			button.registerVariables(variableManager);
			var mod:ModActions = new ModActions(g, this);
			mod.registerTriggers(triggerManager);
			mod.registerVariables(variableManager);
			var sound:SoundTriggers = new SoundTriggers(g, this);
			sound.registerTriggers(triggerManager);
			sound.registerVariables(variableManager);
			
			var him:HimTriggers = new HimTriggers(g, this);
			him.registerTriggers(triggerManager);
			him.registerVariables(variableManager);
		}
		
		private function addCustomVariables():void {
			var clothes:ClothesVariables = new ClothesVariables(g, this);
			clothes.registerVariables(variableManager);
			
			var sdtvar:SDTVariables = new SDTVariables(g, this);
			sdtvar.registerVariables(variableManager);
		}
		
		private function addCustomTriggers():void {
			var bounce:BounceTriggers = new BounceTriggers(g, this);
			bounce.registerTriggers(triggerManager);
			var fade:FadeTriggers = new FadeTriggers(g, this);
			fade.registerTriggers(triggerManager);
			var auto:AutoTriggers = new AutoTriggers(g, this);
			auto.registerTriggers(triggerManager);
			var penis:PenisTriggers = new PenisTriggers(g, this);
			penis.registerTriggers(triggerManager);
			var cleaning:CleaningTriggers = new CleaningTriggers(g, this);
			cleaning.registerTriggers(triggerManager);
			
			var recording:RecordingTriggers = new RecordingTriggers(g, this);
			recording.registerTriggers(triggerManager);
			
			var arm:ArmTriggers = new ArmTriggers(g, this);
			arm.registerTriggers(triggerManager);
			var tongue:TongueTriggers = new TongueTriggers(g, this);
			tongue.registerTriggers(triggerManager);
		}
		
		public function onTrigger():* {
			var x:Boolean = triggerManager.trigger();
			if (x) {
				//displayMessageGreen("onTrigger - Success");
				return x;
			} else {
				//displayMessageWhite("onTrigger - Non DA trigger");
			}
		}
		
		public function onVariableInsert(... args) {
			//displayMessageGreen("onVariableInsert");
			var x = variableManager.variableInsert(args);
			if (x != undefined) {
				return x;
			}
		}
		
		public function onVariableWrite(args:*) {
			//displayMessageGreen("onVariableWrite");
			var x = variableManager.variableWrite(args);
			if (x != undefined) {
				return x;
			}
		}
		
		public function onLineChange(... args):void {
			//displayMessageGreen("onLineChange");
			checkForInstantLine();
		}
		
		public function onDialogueLoad(... args):void {
			//displayMessageGreen("onDialogueLoad");
			lineManager.onDialogueLoad();
		}
		
		public function addLineChangeListenerComms(callbackFunction:Function, callbackTarget:Object, callbackArgs:Array):void {
			addLineChangeListener(new FunctionObject(callbackFunction, callbackFunction, callbackArgs));
		}
		
		public function addLineChangeListener(f:FunctionObject):void {
			lineChangeListeners.push(f);
		}
		
		private function unlockInstantLine(lineName:String, param2:Boolean = false):void {
			currentLine = lineName;
		}
		
		private function lockInstantLine(...args):void {
			currentLine = "";
		}
		
		
		public function checkForInstantLine():void {
			var linelisteners:Array = lineChangeListeners.concat();
			lineChangeListeners = new Array();
			for (var i:uint = 0, isize:uint = linelisteners.length; i < isize; i++) {
				var f:FunctionObject = linelisteners[i];
				f.call();
			}
			var words:Array = g.dialogueControl.words;
			var wordsLength:uint = words.length;
			//Only do instant line if line contains a single trigger
			if (wordsLength == 1) {
				var word = words[0];
				if (word.actionWord) {
					performInstantLine(word.action);
				}
			} else if (StringFunctions.stringEndsWith(currentLine, "_INSTANT")) {
				var allTriggers:Boolean = true;
				for (var i:uint = 0; i < wordsLength && allTriggers; i++) {
					allTriggers = words[i].actionWord;
				}
				if (allTriggers) {
					var queuedBuild:Number = g.dialogueControl.states["queued"]._buildLevel;
					while (g.dialogueControl.sayingWord < wordsLength) {
						g.dialogueControl.nextChar();
					}
					if (queuedBuild < g.dialogueControl.states["queued"]._buildLevel || queuedBuild == g.dialogueControl.states["queued"]._maxBuild) {
						playQueuedLine();
					}
				}
			}
		}
		
		private function playQueuedLine():void {
			g.dialogueControl.states["queued"].clearBuild();
			g.dialogueControl.stopSpeaking();
			g.dialogueControl.sayRandomPhrase(g.dialogueControl.queuedPhrase);
		}
		
		private function performInstantLine(action:String):void {
			var lines:Array = g.dialogueControl.library.getPhrases(action).slice();
			if (lines.length > 0) {
				g.dialogueControl.stopSpeaking(); //she has to stop speaking before you can trigger another line!  It's hidden deeply in SDT - dialogue line canPlay function
				g.dialogueControl.sayRandomPhrase(action);
			}
		}
		
		/*public function onDialogueVariablesReset() {
		   //displayMessageGreen("proxied dialogue data store!");
		   //g.dialogueControl.advancedController._dialogueDataStore = new DictionaryProxy(this);
		}
		
		public function dialogueVariablesInit() {
		   //displayMessageGreen("proxied dialogue data store!");
		   //g.dialogueControl.advancedController._dialogueDataStore = new DictionaryProxy(this, g.dialogueControl.advancedController._dialogueDataStore);
		}*/
		
		private function displayMessage(msg:String, color:String):void {
			main.updateStatusCol("DA: "+msg, color);
		}
		
		public function displayMessageRed(msg:String):void {
			displayMessage(msg, "#FF0000");
		}
		
		public function displayMessageWhite(msg:String):void {
			displayMessage(msg, "#FFFFFF");
		}
		
		public function displayMessageGreen(msg:String):void {
			displayMessage(msg, "#00FF00");
		}
		
		public static function arrayToString(arr:Array):String {
			var str:String = "[";
			var num:Number = arr.length;
			
			for (var i:Number = 0; i < num; i++) {
				var obj:Object = arr[i];
				if (obj is Array) {
					str += arrayToString(arr[i]);
				} else if (obj is String) {
					str += "\"" + arr[i] + "\"";
				} else {
					if (obj is Boolean) {
						if (obj == true) {
							str += "1";
						} else {
							str += "0";
						}
					} else {
						str += obj;
					}
				}
				if ((i + 1) != num) {
					str += ",";
				}
			}
			return str += "]";
		}
		
		public function getGameRef():* {
			return g;
		}
		
		public function getLoaderRef():* {
			return main;
		}
		
		public function getFileReferenceHandler():FileReferenceHandler {
			return fileReferenceHandler;
		}
		
		public function getTriggerManager():TriggerManager {
			return triggerManager;
		}
		
		public function getVariableManager():VariableManager {
			return variableManager;
		}
		
		public function getLineTypeManager():LineTypeManager {
			return lineManager;
		}
		
		public function wwButtonInit():void {
			var y:int = 335;
			for (var i:uint = 0; i < wwButtonArray.length; i++) {
				wwButtonArray[i] = new wwButton_btn();
				main.addInPersist(wwButtonArray[i], main.borders);
				wwButtonArray[i].x = 0;
				wwButtonArray[i].y = y;
				y -= 35;
				wwButtonArray[i].visible = false;
				wwButtonArray[i].buttonText.text = "";
				wwButtonArray[i].buttonText.mouseEnabled = false;
				wwButtonArray[i].addEventListener(MouseEvent.CLICK, new FunctionObject(this.wwButtonPress, this, ["button"+(i+1)]).call);
			}
		}
		
		public function wwButtonPress(button:String, event:MouseEvent):void {
			this.g.dialogueControl.buildState(button, 130);
			getLineTypeManager().resetDialogueClearDelay();
		}
		
		// Functions to load a mod via dialog, called from ModActions.as
		// These seem to be required to be in main.  I couldn't get these functions 
		// to work from within ModActions
		// Added by WeeWillie 4/15/14
		public function loadData(modToLoad:String) {
			var mdl = new modDataLoader(modToLoad, lastLoadedCData, dataLoaded);
			mdl.addEventListener("dataNotFound", dataNotFound);
		}
		
		public function dataNotFound(e) {
			displayMessageRed(e.msg);
			main.unloadMod();
		}
		
		public function dataLoaded(e) {
			var ldr:Loader = new Loader();
			ldr.contentLoaderInfo.addEventListener(Event.INIT, function mDLC(e) {
					main.fileRef = true;
					clearInterval(main.loadInt);
					main.modDataLoadingComplete(e);
				});
			ldr.loadBytes(e.binData);
		}
	}
}
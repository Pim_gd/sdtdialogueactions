﻿package {
	
	import flash.media.Sound;
	
	[Embed(source="../audio/WWSounds_Slap01.swf",symbol="WWSound_Slap01")]
	
	public dynamic class WWSound_Slap01 extends Sound {
		
		public function WWSound_Slap01() {
			super();
		}
	}
}

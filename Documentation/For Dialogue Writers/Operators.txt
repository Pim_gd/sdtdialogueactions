A listing of supported operators and the result with values (10 and 20 used for example):
Operator	var1	var2	output
+		10	20	30 - Adds both sides together.
-		10	20	-10 - Substracts the right side from the left side.
/		10	20	0.5 - Divides the left side by the right side. If the right side is 0, ... then I don't know what happens. Check Adobe's manual for division by 0.
!=		10	20	1 - If both sides don't have the same value, returns 1. Returns 0 otherwise.
==		10	20	0 - If both sides have the same value, returns 1. Returns 0 otherwise.
>=		10	20	0 - If the left side is more than, or equal to the right side, returns 1. Otherwise returns 0.
<=		10	20	1 - If the right side is more than, or equal to the left side, returns 1. Otherwise returns 0.
>		10	20	0 - If the left side is more than the right side, returns 1. Otherwise returns 0.
<		10	20	1 - If the right side is more than the left side, returns 1. Otherwise returns 0.
=		10	20	20 - Whatever the result might have been, it's set to the right side.
&&		1	0	0 - Represents a boolean AND. Returns 1 if (var1 + var2) equals 2. Otherwise, returns 0.
||		1	0	1 - Represents a boolean OR. Returns 1 if (var1 + var2) equals or is more than 1. Otherwise, returns 0.	
%		10	4	2 - Returns the remainder of a division of var1 by var2. Represents a modulo.
\		10	3	3 - Returns the floored value of a division of var1 by var2. Represents integer division.

It is possible to chain operators:
"var1 + var2 - var3"
When you do this, keep in mind that VariableArithmetic does not care about your normal arithmetical order of operators. So "4 + 2 / 2" is 3, not 5.

You can use ( ) to specify ordering (other ordering is ignored).
You can use variables names inside the insertion *var1 + var2* and they will automatically be replaced with the value that they have *var1 + var2* -> *10 + 20* -> 30
Keep in mind that for this to work properly, variable names CANNOT have spaces in them, and variables CANNOT be attached to parentheses or an operator.
Thus, don't use *var1+(var2/var3)*, instead use *var1 + ( var2 / var3 )*.
Additionally, due to the way replacing of insertion values work, variable names CANNOT contain operators.
